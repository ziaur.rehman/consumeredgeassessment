<?php
namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Historical extends Model {
    protected $table = 'historical';

    /**
     * primaryKey
     *
     * @var integer
     * @access protected
     */
    protected $primaryKey = null;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Get the user that owns the phone.
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public static function getHistorical(array $ticker, string $startDate, string $endDate, bool $groupWeekly = false)
    {
        $subQuery = DB::table('historical')
            ->select(
                'd',
                DB::raw('MAX(high) as high'),
                DB::raw('MAX(low) as low'),
                DB::raw('MAX(close) as close')
            )
            ->join('companies', 'companies.company_id', '=', 'historical.company_id')
            ->whereBetween('d', [$startDate, $endDate])
            ->whereIn('companies.ticker', $ticker)
            ->groupBy('d');


        return DB::table('historical', 'h')
            ->join('companies', 'companies.company_id', '=', 'h.company_id')
            ->joinSub($subQuery, 'h1', function ($join)
            {
                $join->on('h.d', '=', 'h1.d');
                $join->on('h.high', '=', 'h1.high');
                $join->on('h.low', '=', 'h1.low');
                $join->on('h.close', '=', 'h1.close');
            })
            ->select(
                'companies.name',
                'companies.ticker',
                'h.d',
                'h.high',
                'h.low',
                'h.close'
            )->get();
    }

    public static function getDailyData(array $ticker, string $startDate, string $endDate, ?bool $groupWeekly = false)
    {
        $hitorical =  DB::table('historical')
            ->select(
                'name',
                'ticker',
                'd',
                'high',
                'low',
                'close'
            )
            ->join('companies', 'companies.company_id', '=', 'historical.company_id')
            ->whereIn('companies.ticker', $ticker)
            ->whereBetween('historical.d', [$startDate, $endDate])
            ->orderBy('d')
            ->orderBy('name')
            ->get();

        if ($groupWeekly) {
            return $hitorical->groupBy(function($data) {
                return Carbon::parse($data->d)->format('W');
            });
        }

        return $hitorical->groupBy('d');
    }
}
