<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Model {
    protected $primaryKey = 'company_id';

    public function historical(): HasMany
    {
        return $this->hasMany(Historical::class, 'company_id', 'company_id');
    }
}
