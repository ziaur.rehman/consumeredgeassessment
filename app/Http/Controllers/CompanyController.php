<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    private const TABLE_NAME = 'companies';

    /**
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        return DB::table(self::TABLE_NAME)->get();
    }


}
