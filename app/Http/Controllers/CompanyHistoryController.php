<?php

namespace App\Http\Controllers;

use App\Models\Historical;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CompanyHistoryController extends Controller
{
    /**
     * @param Request $request
     * @return Collection
     * @throws ValidationException
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'ticker' => 'required|array',
            'start_date' => 'required|date|before:end_date|date_format:Y-m-d',
            'end_date' => 'required|date|after:start_date|date_format:Y-m-d',
            'weekly' => 'nullable|boolean'
        ]);

        return Historical::getHistorical(
            $request->get('ticker'),
            $request->get('start_date'),
            $request->get('end_date')
        );
    }

    public function dailyUpdates(Request $request)
    {
        $this->validate($request, [
            'ticker' => 'required|array',
            'start_date' => 'required|date|before:end_date|date_format:Y-m-d',
            'end_date' => 'required|date|after:start_date|date_format:Y-m-d',
            'weekly' => 'nullable|boolean'
        ]);

        return Historical::getDailyData(
            $request->get('ticker'),
            $request->get('start_date'),
            $request->get('end_date'),
            $request->get('weekly')
        );
    }
}
