## Solution

url: [{{base_url}}/api/v1/searchDaily](#)\
Method: POST\
Request Payload:
```json
{
    "ticker": (array, required, i.e ['UPS', 'TGT']), 
    "start_date": (string, required, i.e '2017-12-30'), 
    "end_date": (string, required, i.e '2017-12-30'), 
    "weekly": (boolean, optional, i.e true|false, set this true to get data by week)
}
```

## Given the following database + data, please build a simple API.

### Requirements
* create an endpoint that accepts date range and ticker(s), returns JSON payload containing company name, ticker, date, high price, low price, closing price, grouped by day
* PHP is preferred

### Optional
* Add optional flags for grouping weekly

### Connection information

```
host: 137.184.195.46:3306
database name: finance
mysql user: finance_user
mysql pass: 13376XENQog682JZQX6TxpQpcoxWN

mysql> describe companies;
+------------+-------------------+------+-----+---------+----------------+
| Field      | Type              | Null | Key | Default | Extra          |
+------------+-------------------+------+-----+---------+----------------+
| company_id | smallint unsigned | NO   | PRI | NULL    | auto_increment |
| ticker     | varchar(5)        | NO   |     | NULL    |                |
| name       | varchar(100)      | NO   |     | NULL    |                |
+------------+-------------------+------+-----+---------+----------------+
3 rows in set (0.00 sec)

mysql> describe historical;
+------------+-------------------+------+-----+---------+-------+
| Field      | Type              | Null | Key | Default | Extra |
+------------+-------------------+------+-----+---------+-------+
| company_id | smallint unsigned | NO   |     | NULL    |       |
| open       | decimal(10,2)     | YES  |     | NULL    |       |
| close      | decimal(10,2)     | YES  |     | NULL    |       |
| high       | decimal(10,2)     | YES  |     | NULL    |       |
| low        | decimal(10,2)     | YES  |     | NULL    |       |
| volume     | int unsigned      | YES  |     | NULL    |       |
| d          | date              | NO   |     | NULL    |       |
+------------+-------------------+------+-----+---------+-------+
7 rows in set (0.00 sec)
```
